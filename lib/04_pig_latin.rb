def consonant?(word)
  vowels = "aeiou".chars
  !vowels.include?(word.chars[0])
end

def vowel?(word)
  vowels = "aeiou".chars
  vowels.include?(word.chars[0])
end

def starting_consts_count(word)
  counter = 0
  word.each_char.with_index do |ch, indx|
    break if vowel?(ch) && word[indx - 1] != "q"
    if consonant?(ch)
      counter += 1
    elsif vowel?(ch) && word[indx - 1] == "q"
      counter += 1
    end
  end
  counter
end

def first_letter(word)
  word[0]
end

def add_ay(word)
  word + "ay"
end

def swtich_consonants(word, amt_of_cnts)
  (word.chars.drop(amt_of_cnts) + word.chars.take(amt_of_cnts)).join
end

def translate_single_word(_string)
  if consonant?(first_letter(_string))
    add_ay(swtich_consonants(_string, starting_consts_count(_string)))
  else add_ay(_string)
  end
end

def translate(_string)
  translation = _string.split.map { |word| translate_single_word(word) }
  translation.join(" ")
end
#shqueop
