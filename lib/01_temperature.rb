require 'byebug'


def conversion_to_celcius(temp_f)
  (5.0 / 9.0) * (temp_f - 32)
end

def conversion_to_farenheit(temp_c)
  (9.0 / 5.0) * temp_c + 32
end

def ftoc(temp_f)
  conversion_to_celcius(temp_f)
end

def ctof(temp_c)
  conversion_to_farenheit(temp_c)
end



if $0 == __FILE__
  puts "conversion successful"
end
