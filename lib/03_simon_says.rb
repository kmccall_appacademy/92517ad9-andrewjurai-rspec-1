def echo(_string)
  _string
end

def shout(_string)
  _string.upcase
end

def repeat(_string, amount = 2)
  word = []
  amount.times { word << _string }
  word.join(" ")
end

def start_of_word(_string, number)
  first_n_letters(_string, number)
end

def first_n_letters(_string, n)
  counter = 0
  output_string = ""

  _string.each_char do |ch|
    if counter != n
      counter += 1
      output_string.concat(ch)
    end
    break if counter == n
  end
  output_string
end

def first_word(_string)
  _string.split[0]
end

def titleize(_string)
  output = caps_all_first_letters(_string)
  output = correct_small_words(output)
  output
end

def caps_all_first_letters(_string)
  _string.split.map(&:capitalize)
end

def correct_small_words(caps_array)
  little_words = ["and", "over", "the"]
  caps_array.map.with_index do |words, indx|
    if little_words.include?(words.downcase) && indx != 0
      words.downcase
    else words
    end
  end.join(" ")
end
