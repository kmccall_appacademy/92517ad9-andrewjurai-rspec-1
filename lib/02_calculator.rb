#02_calculator.rb

def add(int1, int2)
  int1 + int2
end

def subtract(int1, int2)
  int1 - int2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(array)
  array.reduce(:*)
end

def power(number, power)
  number**power
end

def factorial(int)
  return 0 if zero?(int)
  (1..int).reduce(:*)
end

def zero?(int)
  int == 0
end
